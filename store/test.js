export const state = () => ({
    userName: '',
    testId: '',
    resultId: '',
    currentQuestionIdx: null
})

export const mutations = {
    reset(state){
        state.userName = '';
        state.testId = '';
        state.resultId = '';
        state.currentQuestionIdx = null;
    },
    start(state, data){
        state.userName = data.userName;
        state.testId = data.testId;
        state.resultId = data.resultId;
        state.currentQuestionIdx = data.currentQuestionIdx;
    },
    nextQuestion(state, idx){
        state.currentQuestionIdx = idx;
    }
}