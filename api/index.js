'use strict';
const express = require('express');
const app = express();

// do config
require('./config/index')(app);

// set routes
require('./routes/index')(app);

// export the server middleware
module.exports = {
    path: '/api',
    handler: app
}