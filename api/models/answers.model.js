'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var AnswerSchema = new Schema({
    testId: { type: Schema.Types.ObjectId, ref: 'tests' },
    questionId: { type: Schema.Types.ObjectId, ref: 'questions' },
    text: { type: String, required: true }
});

module.exports = AnswerSchema;