'use strict';
module.exports = function (mongoose) {
    // set models
    mongoose.model('Tests', require('./tests.model'));
    mongoose.model('Questions', require('./questions.model'));
    mongoose.model('Answers', require('./answers.model'));
    mongoose.model('TestResults', require('./test-results.model'));

}