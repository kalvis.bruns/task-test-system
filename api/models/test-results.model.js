'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TestResultsSchema = new Schema({
  userName: { type: String, required: true },
  testId: { type: Schema.Types.ObjectId, ref: 'tests', required: true },
  questionCnt: { type: Number, required: true },
  currentQuestionIdx: { type: Number, required: true, default: 1 },
  currentQuestion: { type: Schema.Types.ObjectId, ref: 'questions' },
  result: { type: Number, required: true, default: 0 },
  startDateTime: { type: Date, default: Date.now }
});

module.exports = TestResultsSchema;