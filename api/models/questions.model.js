'use strict';
var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var QuestionSchema = new Schema({
    testId: { type: Schema.Types.ObjectId, ref: 'tests', required: true },
    text: { type: String, required: true },
    idx: { type: Number, required: true },
    correctAnswer: { type: Schema.Types.ObjectId, ref: 'answers', required: true }
});

module.exports = QuestionSchema;