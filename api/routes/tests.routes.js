'use strict';

const testController = require('../controllers/tests.controller');
const testResultsController = require('../controllers/test-results.controller');
const express = require('express');

const router = express.Router();
router.get('', testController.get_all);
router.get('/:testId', testController.get_by_id);
router.get('/:testId/questions/:questionIdx', testController.get_test_answers_by_id);

// Test Results
router.post('/start', testResultsController.start_test);
router.put('/submit', testResultsController.submit_answer);
router.get('/result/:resultId', testResultsController.get_result_by_id);


module.exports = router;