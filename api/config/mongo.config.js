'use strict';
const mongoose = require('mongoose');

//model loading
require('../models/index')(mongoose);
    
mongoose.set('useCreateIndex', true);
// mongoose instance connection url connection
mongoose.Promise = global.Promise;
mongoose.connect('mongodb://localhost:27017/task-test-system', { useNewUrlParser: true });