'use strict';
module.exports = function (app) {
    // mongo config
    require('./mongo.config');

    // set headers
    app.use(require('./headers.config'));

    // set body-parser
    require('./body-parser.config')(app);
}