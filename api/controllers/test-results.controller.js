'use strict';
var mongoose = require('mongoose'),
    TestResults = mongoose.model('TestResults'),
    Questions = mongoose.model('Questions');

exports.start_test = function (req, res) {
    const test_result = new TestResults(req.body);
    test_result.currentQuestionIdx = 1;
    Questions.find({ testId: test_result.testId }, function (err, questions) {
        if (err) return res.send(err);
        if (questions.length > 0) {
            const currQue = questions.find(q => q.idx === test_result.currentQuestionIdx);
            test_result.currentQuestion = currQue;
            test_result.questionCnt = questions.length;
            test_result.save(function (err2, testResult) {
                if (err2) return res.send(err2);
                return res.json(testResult);
            });
        }
    });
};

exports.submit_answer = function (req, res) {
    // find result entry
    TestResults.findById(req.body.resultId, function (err, result) {
        if (err) return res.send(err);
        if (result.currentQuestion == req.body.questionId) {
            // find current question
            Questions.findById(req.body.questionId, function (err2, question) {
                if (err2) return res.send(err2);
                if (question.correctAnswer == req.body.selectedAnswer) {
                    result.result++;
                }
                if (result.currentQuestionIdx >= result.questionCnt) {
                    result.currentQuestion = null;
                    result.currentQuestionIdx = -1;
                    saveResult(result, res);
                } else {
                    // find next question 
                    result.currentQuestionIdx++;
                    Questions.find({ idx: result.currentQuestionIdx, testId: result.testId }, function (err3, questions) {
                        if (err3) return res.send(err3);
                        result.currentQuestion = questions[0];
                        saveResult(result, res);
                    });
                }

            });
        }
    });
};

exports.get_result_by_id = function (req, res) {
    TestResults.findById(req.params.resultId, function (err, result) {
        if (err) return res.send(err);
        return res.json(result);
    });
}

function saveResult(result, res){
    result.save((err, result) => {
        if (err) return res.send(err);
        return res.json(result);
    });
}