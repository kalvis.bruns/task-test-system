'use strict';
var mongoose = require('mongoose'),
    Tests = mongoose.model('Tests'),
    Questions = mongoose.model('Questions'),
    Answers = mongoose.model('Answers');


exports.get_all = function (req, res) {
    Tests.find({}, function (err, tests) {
        if (err) return res.send(err);
        return res.json(tests);
    });
};

exports.get_by_id = function (req, res) {
    Tests.findById(req.params.testId, function (err, test) {
        if (err) return res.send(err);
        return res.json(test);
    });
}

exports.get_test_answers_by_id = function (req, res) {
    const testId = req.params.testId;
    const questionIdx = req.params.questionIdx;
    Tests.findById(testId, function (err, test) {
        if (err) return res.send(err);
        Questions.find({ testId: test._id }, function (err2, allQuestions) {
            if (err2) return res.send(err2);
            Questions.find({ idx: questionIdx, testId: test._id }, function (err3, questions) {
                if (err3) return res.send(err3);
                const question = questions[0];
                if (question) {
                    Answers.find({ questionId: question._id, testId: test._id }, function (err4, answers) {
                        if (err4) return res.send(err4);
                        return res.json({
                            test: test,
                            question: question,
                            questionCnt: allQuestions.length,
                            answers: answers
                        });
                    });
                }
            });
        });
    });
}
