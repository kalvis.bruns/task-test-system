const pkg = require('./package')


module.exports = {
  mode: 'spa',
  /*
  ** Headers of the page
  */
  head: {
    title: pkg.name,
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#fff' },

  /*
  ** Global CSS
  */
  css: [
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    '~/plugins/vue-notifications.js'
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [,
    '@nuxtjs/axios',
    'bootstrap-vue/nuxt',
    'vue-router',
  ],

  env: {
    apiUrl: 'http://localhost:3000/api/'
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      
    }
  },

  axios: {
    // proxyHeaders: false
  },

  serverMiddleware: ['~/api/index.js']
}
