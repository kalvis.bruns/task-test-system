# Task-Test-System

A simple test-quiz system built on Nuxt.js framework. My first project using Vue.js frontend framework.  

## Dependencies

### NPM dependencies
* @nuxtjs/axios
* body-parser
* bootstrap
* bootstrap-vue
* cross-env
* express
* mongoose
* nuxt
* vue-notification
* vuex

### Other dependencies
* MongoDB

## Build Setup

``` bash
# install dependencies
$ npm install

# serve with hot reload at localhost:3000
$ npm run dev

# build for production and launch server
$ npm run build
$ npm start

# generate static project
$ npm run generate
```

## System architecture

### API

Folder 'api' contains backend API service (Node.js + ExpressJs). Connection and data exchange between ExpressJs and MongoDB is made using Mongoose schemas.
File 'index.js' is the entry point for the backend server.  

Folder descriptions:

* config - server configuration files.
* controllers - controller functions, executed for the specific API routes 
* models - Mongoose data model schemas for document mapping between MongoDB and tha ExpressJs server
* routes - API route definitions and calling the according controller functions

### Layouts

Layouts folder contains default layout definition for the pages.

### MongoDB structure

Folder 'mongo-dump' contains data dump from Mongo DB to the JSON format.

* tests.json - contents of the tests document collection. One document represents a single test.
* questions.json - contents of the questions document collection. One document represents a single question for a test. Holds link to the correct answer. 
* answers.json - contents of the ansers document collection. One document represents a single answer for a question.
* testresults.json - contents of the testresults document collection. One document represents a finished or ongoing test filling. Property 'result' holds the number of correct answers.

![DB relation diagram](static/rel_diagram.png)

### App pages

Folder 'pages' contains page definitions for the app. App has three pages.

* index.vue - a start page. User can enter his name and select a test from the list.
* test.vue - a test question page. User can read the question, select an answer and submit his choice by moving on to the next question, or moving to the results page.
* results.vue - a results page. User can see his name, number of the correct answers and the total count of the questions in the test.

### Plugins

Folder 'plugins' contains definitions of the additional plugins added to the app. Added notifications package 'vue-notification' witch enables possibility to display notifications to the user. For example in the start page, if user has not selected a test, or has not entered his name, then the notification appears when he clicks on a 'Begin' button. The similar notification is displayed when user has not selected an answer to the question, and wants to move on to the next question.

### Store

Folder 'store' contains state definition and mutation functions for the defined substore. Test sub-store holds folowing info:

* user name
* id of the test document in the Mongo DB
* id of the testresults document in the Mongo DB
* index of the current question for the selected test

Store gets updated once user starts doing selected test and after submitting an answer for each question. 

## Author

* **Kalvis Brūns**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details